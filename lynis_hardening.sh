#
# run the following command:
# #lynis audit system
#
# And, here are the suggestions from Lynis
# 
timestamp="`date +%Y-%m-%d_%H-%M-%S`"

Postfix_information_leakage()
{
	echo '|Postfix information leakage'

	#Postfix Disable Network Listening
	echo '| -Change the "smtpd_banner" in main.cf to have the banner only show ESMTP. (see https://cisofy.com/controls/MAIL-8818/)'
	echo '		e.g. find the following lines '
	echo '			#smtpd_banner = $myhostname ESMTP $mail_name'
	echo '			#smtpd_banner = $myhostname ESMTP $mail_name ($mail_version)'
	echo '		and add the following line'
	echo '			smtpd_banner = ESMTP'
	gedit  /etc/postfix/main.cf
}
Single_user_mode_for_systemd()
{
	echo '|- Protect rescue.service by using sulogin [BOOT-5260] (https://cisofy.com/controls/BOOT-5260/)'
	echo '		e.g. replace the following line'
	echo '			ExecStart=-/bin/sh -c "/usr/sbin/sulogin; /usr/bin/systemctl --fail --no-block default"'
	echo '		with the following line'
	echo '			ExecStart=-/usr/sbin/sulogin'
	gedit /usr/lib/systemd/system/rescue.service
}
Check_Deleted_Files()
{
	echo '|Check what deleted files are still in use and why'
	echo '|Deleted files:'
	lsof | grep -i "delete"
	
	echo '|Press any key to continue ...'
	read -n1 kbd


}
Add_legal_banner()
{
	echo '| Add legal banner to warn unauthorized users'

	echo 'Add legal banner to /etc/issue.net, to warn unauthorized users'
	echo 'You are not allowed to access this machine.' >> /etc/motd
	echo 'Note: You are not allowed to access this machine.' >> /etc/issue
	echo 'You are not allowed to access this machine from remote.' >> /etc/issue.net
}
On_Sysstat()
{
	echo '|Enable sysstat to collect accounting '
	yum install sysstat
	pidstat	
	echo '|Press any key to continue ...'
	read -n1 kbd

	mpstat	
	echo '|Press any key to continue ...'
	read -n1 kbd

	iostat	
	echo '|Press any key to continue ...'
	read -n1 kbd

	sadf	
	echo '|Press any key to continue ...'
	read -n1 kbd

	sar	
	echo '|Press any key to continue ...'
	read -n1 kbd
}
#--------------------------------------------------------------------
echo '';echo '';echo '';
Postfix_information_leakage;
Single_user_mode_for_systemd;
Check_Deleted_Files;
Add_legal_banner;
On_Sysstat;






