/*global define, is_chrome_two*/
/*jslint browser: true*/

/**
 * This module is used to triggers pageview analytics.
 *
 * @module analytics/pageview
 * @author Michael Clayton <mclayton@redhat.com>
 * @copyright Red Hat 2014
 */
define([
    'underscore',
    'analytics/main',
    'analytics/attributes',
    'google_remarketing',
    'chrome_lib'
], function (
    _,
    paf,
    attributes,
    g_remark,
    lib
) {

    "use strict";

    var CLIENT_CHROME_DELAY = 350; // ms

    /**
     * This function tests to see whether basic analytics (a simple PAF
     * pageview report) has been run on the current page.  If it hasn't, it
     * runs it after a short delay.
     */
    function run_once() {
        if (is_chrome_two()) {
            // wait for the user status service to return, then send analytics
            // if they aren't on the page.
            lib.whenUserStatusReady(
                _.partial(_.delay, run, CLIENT_CHROME_DELAY)
            );
        } else {
            run();
        }
    }

    /**
     * Run basic pageview analytics.
     */
    function run() {
        try {
            paf.init(['omniture', 'eloqua']);
            attributes.harvest();

            if (paf.auto_report_on()) {
                paf.report();
            }

            g_remark.add_tag();
        } catch (e) {
            // most likely we wind up here because of an anti-tracking browser
            // extension.  Honor it.
        }
    }

    return {
        run_once : run_once,
        send     : run
    };

});

