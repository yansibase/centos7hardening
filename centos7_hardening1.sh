# centos7_hardening1.sh
#

timestamp="`date +%Y-%m-%d_%H-%M-%S`"

# backup this script
cp -p /home/user0/Documents/centos7_hardening1.sh /run/media/user0/HDDREG/tools/centos7_hardening1.sh

gLogFilePath="./centos7_hardening1.log"
echo "Begin" > $gLogFilePath

backupFile()
{
	originalFilePath="$1";

	if [ -f "$originalFilePath" ]
	then
		dir=`dirname $originalFilePath`;
		base=`basename $originalFilePath`;
		# cp command can't copy the file to the source directory, 
		# so I copy the source file to /tmp and then move it back with timestamp in 
		# file name  to source directory.
		# copy the file to /tmp
		cp -p $originalFilePath /tmp;

		# move the /tmp/$file back to src dir
		mv -f "/tmp/$base" "$originalFilePath-$timestamp";	
	else
		echo "$originalFilePath not found."
	fi
}

Setup_accunts()
{
	echo "| Setup_accunts"
	#account setup
	passwd -l xfs
	passwd -l news
	passwd -l nscd
	passwd -l dbus
	passwd -l vcsa
	passwd -l games
	passwd -l nobody
	passwd -l avahi
	passwd -l haldaemon
	passwd -l gopher
	passwd -l ftp
	passwd -l mailnull
	passwd -l pcap
	passwd -l mail
	passwd -l shutdown
	passwd -l halt
	passwd -l uucp
	passwd -l operator
	passwd -l sync
	passwd -l adm
	passwd -l lp
}

Remove_App()
{
	echo "| Remove_App"
	/etc/rc.d/init.d/apmd stop
	/etc/rc.d/init.d/sendmail stop
	/etc/rc.d/init.d/kudzu stop

	rpm  -e  pump
	rpm  -e  apmd
	rpm  -e  lsapnptools
	rpm  -e  redhat-logos
	rpm  -e  mt-st
	rpm  -e  kernel-pcmcia-cs
	rpm  -e  setserial
	rpm  -e  redhat-relese
	rpm  -e  eject
	rpm  -e  linuxconf
	rpm  -e  kudzu
	rpm  -e  gd
	rpm  -e  bc
	rpm  -e  getty_ps
	rpm  -e  raidtools
	rpm  -e  pciutils
	rpm  -e  mailcap
	rpm  -e  setconsole
	rpm  -e  gnupg

	
	chkconfig postfix off # echo "close Mail   Server "
	chkconfig --level 35 apmd off
	chkconfig --level 35 netfs off
	chkconfig --level 35 yppasswdd off
	chkconfig --level 35 ypserv off
	chkconfig --level 35 dhcpd off?
	chkconfig --level 35 portmap off
	chkconfig --level 35 lpd off
	chkconfig --level 35 nfs off
	chkconfig --level 35 sendmail off
	chkconfig --level 35 snmpd off
	chkconfig --level 35 rstatd off
	chkconfig --level 35 atd off
}
Remove_User()
{
	echo "| Remove_User"

	userdel adm
	userdel lp
	userdel sync
	userdel shutdown
	userdel halt
	userdel news
	userdel uucp
	userdel operator
	userdel games
	userdel gopher
	userdel ftp

	groupdel adm
	groupdel lp
	groupdel news
	groupdel uucp
	groupdel games
	groupdel dip

	chmod 0755 /etc/passwd
	chmod 0755 /etc/shadow
	chmod 0755 /etc/group
	chmod 0755 /etc/gshadow
	chattr +i /etc/passwd
	chattr +i /etc/shadow
	chattr +i /etc/group
	chattr +i /etc/gshadow

	chmod 600  /etc/services
	chown root /etc/services
	chattr +i  /etc/services

	# /etc, /usr/etc, /bin, /usr/bin, /sbin, /usr/sbin, /tmp and/var/tmp的属主是root,并且设置粘滞
	chown root /etc
	chown root /usr/etc
	chown root /bin
	chown root /usr/bin
	chown root /sbin
	chown root /usr/sbin
	chown root /tmp and/var/tmp
	chmod +t /etc
	chmod +t /usr/etc
	chmod +t /bin
	chmod +t /usr/bin
	chmod +t /sbin
	chmod +t /usr/sbin
	chmod +t /tmp and/var/tmp



	# 只有根用户允许在该目录下使用 Read、Write，和 Execute 脚本文件
	chmod -R 700 /etc/rc.d/init.d/* 
	chmod -R 700 /etc/init.d/*

	# limit chmod important commands
	chmod 700 /bin/ping
	chmod 700 /usr/bin/finger
	chmod 700 /usr/bin/who
	chmod 700 /usr/bin/w
	chmod 700 /usr/bin/locate
	chmod 700 /usr/bin/whereis
	chmod 700 /sbin/ifconfig
	chmod 700 /usr/bin/pico
	chmod 700 /bin/vi
	chmod 700 /usr/bin/which
	#chmod 700 /usr/bin/gcc
	#chmod 700 /usr/bin/make
	chmod 700 /bin/rpm

	# Narrow Down Permissions
	chmod 700 /root
	chmod 700 /var/log/audit
	chmod 740 /etc/rc.d/init.d/iptables
	chmod 740 /sbin/iptables
	chmod -R 700 /etc/skel
	chmod 600 /etc/rsyslog.conf
	chmod 640 /etc/security/access.conf
	chmod 600 /etc/sysctl.conf


	# history security
	chattr +a /root/.bash_history
	chattr +i /root/.bash_history

	chmod 600 /etc/grub.conf
	chattr +i /etc/grub.conf
}

Disable_Ping_Response()
{
	echo "| Disable_Ping_Response"

	echo 1 > /proc/sys/net/ipv4/icmp_echo_ignore_all
	
	#将上述命令加到/etc/rc.d/rc.local中去，每次重启动将自动执行
	filepath="/etc/rc.d/rc.local"
	if [ -f "$filepath" ]
	then
		echo "$filepath found."
	else
		echo "$filepath not found."
		touch $filepath
	fi
	echo "echo 1 > /proc/sys/net/ipv4/icmp_echo_ignore_all" >> $filepath
}

Disable_IP_Source_Routing()
{
	echo "| Disable_IP_Source_Routing"

	for f in /proc/sys/net/ipv4/conf/*/accept_source_route; do
	#echo $f
	#cat $f
	echo 0 > $f
	done

	#将上述命令加到/etc/rc.d/rc.local中去，每次重启动将自动执行
	filepath="/etc/rc.d/rc.local"
	if [ -f "$filepath" ]
	then
		echo "$filepath found."
	else
		echo "$filepath not found."
		touch $filepath
	fi
	echo "for f in /proc/sys/net/ipv4/conf/*/accept_source_route; do" >> $filepath
	echo "echo 0 > $f" >> $filepath
	echo "done"        >> $filepath
}
Res_limits()
{
	echo "prevent Dos attack"
	# in On_core_dumps() in centos7_hardening2.sh
	# 禁止创建core文件
	#echo "* hard core 0"   >> /etc/security/limits.conf
	# 除root外，其他用户最多使用5M内存
	#echo "* hard rss 5000" >> /etc/security/limits.conf
	# 最多进程数限制为20
	#echo "* hard nproc 20" >> /etc/security/limits.conf

	echo "session required /lib/security/pam_limits.so" >> /etc/pam.d/login
}
File_Rights()
{

	echo "查找任何人可写的文件和目录" >> $gLogFilePath
	echo "find / -type f \( -perm -2 -o -perm -20 \) -exec ls -lg {} \;" >> $gLogFilePath
	      find / -type f \( -perm -2 -o -perm -20 \) -exec ls -lg {} \;  >> $gLogFilePath
	echo "find / -type d \( -perm -2 -o -perm -20 \) -exec ls -ldg {} \;">> $gLogFilePath
	      find / -type d \( -perm -2 -o -perm -20 \) -exec ls -ldg {} \; >> $gLogFilePath
	
	echo "查找异常文件, 如..文件,...文件等"            >> $gLogFilePath
	echo "find / -name ".." -print -xdev"          >> $gLogFilePath
	      find / -name ".." -print -xdev	       >> $gLogFilePath
	echo "find / -name ".*" -print -xdev | cat -v" >> $gLogFilePath
	      find / -name ".*" -print -xdev | cat -v  >> $gLogFilePath

	echo "检查没有属主的文件"            >> $gLogFilePath
	echo "find / -nouser -o -nogroup" >> $gLogFilePath
	      find / -nouser -o -nogroup  >> $gLogFilePath

	echo "检查在/dev目录以外还有没有特殊的块文件"                          >> $gLogFilePath
	echo "find / \( -type b -o -type c \) -print | grep -v '^/dev/'" >> $gLogFilePath
	      find / \( -type b -o -type c \) -print | grep -v '^/dev/'  >> $gLogFilePath



}

remove_logon_msg()
{
	echo "remove_logon_msg"
	rm -f /etc/issue
	rm -f /etc/issue.net
	touch /etc/issue
	touch /etc/issue.net
}

prevent_IP_cheat()
{
	echo "prevent_IP_cheat"

	backupFile /etc/host.conf

	echo "order bind，hosts"	>  /etc/host.conf
	echo "multi off" 		>> /etc/host.conf
	echo "nospoof on"		>> /etc/host.conf
}

Close_SSH_Srver()
{
	echo '| Close SSH server'
	
	echo 'systemctl stop sshd.service' >> '/root/.bashrc'
	source /root/.bashrc

	systemctl status sshd.service
	echo '|Make sure sshd is stopped'
	echo '|Press any key to continue ...'
	read -n1 kbd

}
##########################################################################
echo '';echo '';echo ''
echo '-------------------------------------------'
echo 'Security Harden CentOS 7    1'
echo '-------------------------------------------'

echo '';echo '';echo ''
Setup_accunts;
echo '';echo '';echo ''
Remove_App;
echo '';echo '';echo ''
Remove_User;
echo '';echo '';echo ''
Disable_Ping_Response;
echo '';echo '';echo ''
Disable_IP_Source_Routing;
echo '';echo '';echo ''
Res_limits;
echo '';echo '';echo ''
File_Rights;
echo '';echo '';echo ''
remove_logon_msg;
echo '';echo '';echo ''
prevent_IP_cheat;
echo '';echo '';echo ''
Close_SSH_Srver;
