var MiNS = new Object();
MiNS.G = {
    domain: "120.132.72.17:8088/net/webjs/upload",
    maxcount: 1,
    count: 1,
    alltime: 0,
    turl: "",
    tnode: "",
    tsize: "",
    itype: "3",
    ntype: "2",
    random: 40,
    isBase64: true,

    idc:[{"n":"1","i":["电信"],"v":"http://183.57.73.126/noc.gif","l":10},{"n":"3","i":["电信"],"v":"http://121.14.34.49/noc.gif","l":10},{"n":"4","i":["电信"],"v":"http://219.135.97.251/noc.gif","l":10},{"n":"6","i":["移动"],"v":"http://183.232.160.28/noc.gif","l":10},{"n":"29","i":["第三方"],"v":"http://120.132.36.28/noc.gif","l":10},{"n":"30","i":["电信"],"v":"http://180.97.82.20/noc.gif","l":10},{"n":"31","i":["BGP"],"v":"http://114.67.63.82/noc.gif","l":10},{"n":"37","i":["电信"],"v":"http://117.25.144.105/noc.gif","l":10},{"n":"38","i":["电信"],"v":"http://218.5.77.205/noc.gif","l":10},{"n":"39","i":["联通"],"v":"http://113.207.27.3/noc.gif","l":10},{"n":"43","i":["电信"],"v":"http://61.188.176.138/noc.gif","l":10},{"n":"57","i":["移动"],"v":"http://117.156.241.133/noc.gif","l":10},{"n":"58","i":["移动"],"v":"http://211.138.238.247/noc.gif","l":10},{"n":"59","i":["移动"],"v":"http://111.23.2.85/noc.gif","l":10},{"n":"60","i":["电信"],"v":"http://219.148.162.142/noc.gif","l":10},{"n":"61","i":["联通"],"v":"http://118.212.135.34/noc.gif","l":10},{"n":"63","i":["电信"],"v":"http://121.205.89.247/noc.gif","l":10},{"n":"64","i":["电信"],"v":"http://59.56.104.37/noc.gif","l":10},{"n":"65","i":["电信"],"v":"http://59.56.101.138/noc.gif","l":10},{"n":"67","i":["联通"],"v":"http://221.3.133.21/noc.gif","l":10},{"n":"69","i":["移动"],"v":"http://211.140.23.241/noc.gif","l":10},{"n":"73","i":["联通"],"v":"http://61.158.237.2/noc.gif","l":10},{"n":"74","i":["联通"],"v":"http://124.95.164.211/noc.gif","l":10},{"n":"75","i":["联通"],"v":"http://122.192.33.10/noc.gif","l":10},{"n":"76","i":["联通"],"v":"http://221.6.95.2/noc.gif","l":10},{"n":"80","i":["联通"],"v":"http://113.200.88.51/noc.gif","l":10},{"n":"81","i":["电信"],"v":"http://115.238.21.230/noc.gif","l":10},{"n":"82","i":["电信"],"v":"http://202.100.85.123/noc.gif","l":10},{"n":"83","i":["移动"],"v":"http://120.132.36.28/noc.gif","l":10},{"n":"102","i":["电信"],"v":"http://58.222.24.186/noc.gif","l":10},{"n":"113","i":["电信"],"v":"http://218.95.38.85/noc.gif","l":10},{"n":"114","i":["移动"],"v":"http://221.180.21.22/noc.gif","l":10},{"n":"115","i":["电信"],"v":"http://113.105.159.145/noc.gif","l":10},{"n":"116","i":["联通"],"v":"http://42.236.61.228/noc.gif","l":10},{"n":"117","i":["联通"],"v":"http://125.211.195.17/noc.gif","l":10},{"n":"118","i":["电信"],"v":"http://219.232.241.60/noc.gif","l":10},{"n":"119","i":["电信"],"v":"http://116.8.117.92/noc.gif","l":10},{"n":"120","i":["移动"],"v":"http://117.131.215.21/noc.gif","l":10},{"n":"128","i":["联通"],"v":"http://-/noc.gif","l":10}],
    nsc:[
    ]
};

MiNS.ni_onload = function() {
    var C = (new Date()).getTime() - this.stime;
    MiNS.G.alltime = MiNS.G.alltime + C;
    if (MiNS.G.count < MiNS.G.maxcount) {
        MiNS.G.count++;
        MiNS.TestUrl(MiNS.G.turl, MiNS.G.tnode, this.nitype);
        return
    } else {
        var A = new Image(0, 0),
            B = (this.nitype == MiNS.G.itype) ? MiNS.iUrl: MiNS.nUrl,
            base64 = new MiNS.Base64(),
            srcStrings = "\"Node\":\"" + this.node + "\",\"Ok\":\"1\",\"T\":\"" + Math.floor(MiNS.G.alltime / MiNS.G.maxcount) + "\",\"U\":\"" + this.UCookie + "\",\"L\":\"" + this.size + "\",\"I\":\"" + this.ip + "\",\"Nocache\":\"" + Math.random()+"\"}";

        A.src = B + base64.encode(srcStrings);
    }
};

MiNS.ni_onerror = function() {
    var A = new Image(0, 0),
        B = (this.nitype == MiNS.G.itype) ? MiNS.iUrl: MiNS.nUrl,
        base64 = new MiNS.Base64(),
        srcStrings = "\"Node\":\"" + this.node + "\",\"Ok\":\"0\",\"T\":\"" + ((new Date()).getTime() - this.stime) + "\",\"U\":\"" + this.UCookie + "\",\"L\":\"" + this.size + "\",\"I\":\"" + this.ip + "\",\"Nocache\":\"" + Math.random()+"\"}";
    A.src = B + base64.encode(srcStrings);
};
/**
 * [TestUrl description]
 * @param {[type]} B [图片的URL地址]
 * @param {[type]} D [图片的HOST]
 * @param {[type]} C [ 2 or 3 ]
 */
MiNS.TestUrl = function(B, D, C, S, I) {
    var A = new Image();
    A.node = D;
    A.onload = MiNS.ni_onload;
    A.onerror = MiNS.ni_onerror;
    A.stime = (new Date()).getTime();
    A.nitype = C;
    A.size = S;
    A.UCookie = MiNS.getCookieUID();
    A.ip = I;

    A.src = B + "?" + Math.random()
};

/**
 * [base64加密解密]
 * @return { String }
 */
MiNS.Base64 = function () {

    // 私有属性
    _keyStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=,";

    // 公共方法 ENCODE 加密
    this.encode = function (input) {
        if(!MiNS.G.isBase64){
            return input;
        }

        var output = "";
        var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
        var i = 0;
        input = _utf8_encode(input);
        while (i < input.length) {
            chr1 = input.charCodeAt(i++);
            chr2 = input.charCodeAt(i++);
            chr3 = input.charCodeAt(i++);
            enc1 = chr1 >> 2;
            enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
            enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
            enc4 = chr3 & 63;
            if (isNaN(chr2)) {
                enc3 = enc4 = 64;
            } else if (isNaN(chr3)) {
                enc4 = 64;
            }
            output = output +
            _keyStr.charAt(enc1) + _keyStr.charAt(enc2) +
            _keyStr.charAt(enc3) + _keyStr.charAt(enc4);
        }
        return output;
    }

    // 私有方法 UTF-8 编码
    _utf8_encode = function (string) {
        string = string.replace(/\\r\\n/g,"\\n");
        var utftext = "";
        for (var n = 0; n < string.length; n++) {
            var c = string.charCodeAt(n);
            if (c < 128) {
                utftext += String.fromCharCode(c);
            } else if((c > 127) && (c < 2048)) {
                utftext += String.fromCharCode((c >> 6) | 192);
                utftext += String.fromCharCode((c & 63) | 128);
            } else {
                utftext += String.fromCharCode((c >> 12) | 224);
                utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                utftext += String.fromCharCode((c & 63) | 128);
            }

        }
        return utftext;
    }
    // public method for decoding
    this.decode = function (input) {
        var output = "";
        var chr1, chr2, chr3;
        var enc1, enc2, enc3, enc4;
        var i = 0;
        input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");
        while (i < input.length) {
            enc1 = _keyStr.indexOf(input.charAt(i++));
            enc2 = _keyStr.indexOf(input.charAt(i++));
            enc3 = _keyStr.indexOf(input.charAt(i++));
            enc4 = _keyStr.indexOf(input.charAt(i++));
            chr1 = (enc1 << 2) | (enc2 >> 4);
            chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
            chr3 = ((enc3 & 3) << 6) | enc4;
            output = output + String.fromCharCode(chr1);
            if (enc3 != 64) {
                output = output + String.fromCharCode(chr2);
            }
            if (enc4 != 64) {
                output = output + String.fromCharCode(chr3);
            }
        }
        output = _utf8_decode(output);
        return output;
    }

    // private method for UTF-8 decoding
    _utf8_decode = function (utftext) {
        var string = "";
        var i = 0;
        var c = c1 = c2 = 0;
        while ( i < utftext.length ) {
            c = utftext.charCodeAt(i);
            if (c < 128) {
                string += String.fromCharCode(c);
                i++;
            } else if((c > 191) && (c < 224)) {
                c2 = utftext.charCodeAt(i+1);
                string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
                i += 2;
            } else {
                c2 = utftext.charCodeAt(i+1);
                c3 = utftext.charCodeAt(i+2);
                string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
                i += 3;
            }
        }
        return string;
    }
};

MiNS.cookie = function(key, value, options) {
    if (arguments.length > 1 && String(value) !== "[object Object]") {
        if (value === null || value === undefined) {
            options.expires = -1;
        }
        if (typeof options.expires === 'number') {
            var days = options.expires, t = options.expires = new Date();
            t.setDate(t.getDate() + days);
        }
        value = String(value);
        return (document.cookie = [
            encodeURIComponent(key), '=',
            options.raw ? value : encodeURIComponent(value),
            options.expires ? '; expires=' + options.expires.toUTCString() : '',
            options.path ? '; path=' + options.path : '',
            options.domain ? '; domain=' + options.domain : '',
            options.secure ? '; secure' : ''
        ].join(''));
    }
    options = value || {};
    var result, decode = options.raw ? function (s) { return s; } : decodeURIComponent;
    return (result = new RegExp('(?:^|; )' + encodeURIComponent(key) + '=([^;]*)').exec(document.cookie)) ? decode(result[1]) : null;
};
MiNS.getCookieUID = function(){
    var cookieStr = this.cookie('userId') ? this.cookie('userId') : "";
    return cookieStr;
};
MiNS.Init = function() {

    //var B = Math.floor(10 * Math.random()) + 1;
    /*if (B != 1) {
        return
    }*/

   // var B = Math.floor( Math.random() * 100 );
   // if( B < 0 || B >= MiNS.G.random ){
   //    return;
   // }

    var base64 = new MiNS.Base64();

    MiNS.iUrl = "http://" + MiNS.G.domain + "?" + base64.encode("{\"Type\":\"" + MiNS.G.itype + "\",");
    MiNS.nUrl = "http://" + MiNS.G.domain + "?" + base64.encode("{\"Type\":\"" + MiNS.G.ntype + "\",");

    var A = Math.floor((MiNS.G.idc.length + MiNS.G.nsc.length) * Math.random());//小于等于121随机整数

    var D = A - MiNS.G.nsc.length;


    //var E = Math.floor(MiNS.G.ip.length * Math.random());//小于MiNS.G.ip.length随机整数


    if (D < 0) {
        var E = Math.floor(MiNS.G.nsc[A].i.length * Math.random());//小于MiNS.G.nsc[A].i.length随机整数

        //MiNS.G.turl = MiNS.G.nsc[A].v.replace(MiNS.G.nsc[A].n , MiNS.G.nsc[A].i[E]);//图片URL地址
        MiNS.G.turl = MiNS.G.nsc[A].v;//图片URL地址
        MiNS.G.tnode = MiNS.G.nsc[A].n;//图片HOST
        MiNS.G.tsize = MiNS.G.nsc[A].l;//图片SIZE大小
        MiNS.TestUrl(MiNS.G.turl, MiNS.G.tnode, MiNS.G.ntype, MiNS.G.tsize , MiNS.G.nsc[A].i[E])//B:图片的URL地址,D:图片的HOST,C:2

    } else {
        var E = Math.floor(MiNS.G.idc[D].i.length * Math.random());//小于MiNS.G.idc[D].i.length随机整数

        //MiNS.G.turl = MiNS.G.idc[D].v.replace(MiNS.G.idc[D].n , MiNS.G.idc[D].i[E]);//图片URL地址
        MiNS.G.turl = MiNS.G.idc[D].v;
        MiNS.G.tnode = MiNS.G.idc[D].n;//图片HOST
        MiNS.G.tsize = MiNS.G.idc[D].l;//图片SIZE大小
        MiNS.TestUrl(MiNS.G.turl, MiNS.G.tnode, MiNS.G.itype, MiNS.G.tsize , MiNS.G.idc[D].i[E])//B:图片的URL地址,D:图片的HOST,C:2
    }
};
try {
    MiNS.Init();
} catch(e) {}
