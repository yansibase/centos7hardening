
# os_protection.sh
# https://wiki.centos.org/HowTos/OS_Protection

timestamp="`date +%Y-%m-%d_%H-%M-%S`"

Modifying_fstab()
{
	echo "Modifying_fstab"
# call harden_etc_fstab.sh
}

Package_installs()
{
	echo "Package_installs"

	yum list installed > ~/installed_packages.txt
}

single_user_mode()
{
	echo "single_user_mode"

	echo "# Require the root pw when booting into single user mode" >> /etc/inittab
	echo "~~:S:wait:/sbin/sulogin" >> /etc/inittab
	echo "Don't allow any nut to kill the server"
	perl -npe 's/ca::ctrlaltdel:\/sbin\/shutdown/#ca::ctrlaltdel:\/sbin\/shutdown/' -i /etc/inittab
}

protect_grub()
{
	echo 'When prompted, type the GRUB password and press Enter'
	/sbin/grub-md5-crypt

	echo 'Open the file and below the timeout line in the main section of the document, add the following line:
	password --md5 <password-hash>
	Replace <password-hash> with the value returned by /sbin/grub-md5-crypt'
	vim /boot/grub/grub.conf
}

Restricting_Root()
{
	echo 'Restricting_Root'
	echo "tty1" > /etc/securetty
	chmod 700 /root
}

Password_Policies()
{
	echo 'Password_Policies'

	echo "Passwords expire every 180 days"
	perl -npe 's/PASS_MAX_DAYS\s+99999/PASS_MAX_DAYS 180/' -i /etc/login.defs
	echo "Passwords may only be changed once a day"
	perl -npe 's/PASS_MIN_DAYS\s+0/PASS_MIN_DAYS 1/g' -i /etc/login.defs
}
password_protection_sha512()
{
	echo 'password_protection_sha512'
	authconfig --passalgo=sha512 --update
}
Umask_restrictions()
{
	echo 'Umask_restrictions'
	perl -npe 's/umask\s+0\d2/umask 077/g' -i /etc/bashrc
	perl -npe 's/umask\s+0\d2/umask 077/g' -i /etc/csh.cshrc
}
Reaping_idle_users()
{
	echo 'Reaping_idle_users'
	echo "Idle users will be removed after 15 minutes"
	echo "readonly TMOUT=900" >> /etc/profile.d/os-security.sh
	echo "readonly HISTFILE" >> /etc/profile.d/os-security.sh
	chmod +x /etc/profile.d/os-security.sh
}
Restricting_cron_and_at()
{
	echo 'Restricting_cron_and_at'
	echo "Locking down Cron"
	touch /etc/cron.allow
	chmod 600 /etc/cron.allow
	awk -F: '{print $1}' /etc/passwd | grep -v root > /etc/cron.deny
	echo "Locking down AT"
	touch /etc/at.allow
	chmod 600 /etc/at.allow
	awk -F: '{print $1}' /etc/passwd | grep -v root > /etc/at.deny
}
Sysctl_Security()
{
	echo 'Sysctl Security'

	echo 'net.ipv4.ip_forward = 0'				>>  /etc/sysctl.conf
	echo 'net.ipv4.conf.all.send_redirects = 0'		>>  /etc/sysctl.conf
	echo 'net.ipv4.conf.default.send_redirects = 0'		>>  /etc/sysctl.conf
	echo 'net.ipv4.tcp_max_syn_backlog = 1280'		>>  /etc/sysctl.conf
	echo 'net.ipv4.icmp_echo_ignore_broadcasts = 1'		>>  /etc/sysctl.conf
	echo 'net.ipv4.conf.all.accept_source_route = 0'	>>  /etc/sysctl.conf
	echo 'net.ipv4.conf.all.accept_redirects = 0'		>>  /etc/sysctl.conf
	echo 'net.ipv4.conf.all.secure_redirects = 0'		>>  /etc/sysctl.conf
	echo 'net.ipv4.conf.all.log_martians = 1'		>>  /etc/sysctl.conf
	echo 'net.ipv4.conf.default.accept_source_route = 0'	>>  /etc/sysctl.conf
	echo 'net.ipv4.conf.default.accept_redirects = 0'	>>  /etc/sysctl.conf
	echo 'net.ipv4.conf.default.secure_redirects = 0'	>>  /etc/sysctl.conf
	echo 'net.ipv4.icmp_echo_ignore_broadcasts = 1'		>>  /etc/sysctl.conf
	echo 'net.ipv4.icmp_ignore_bogus_error_responses = 1'	>>  /etc/sysctl.conf
	echo 'net.ipv4.tcp_syncookies = 1'			>>  /etc/sysctl.conf
	echo 'net.ipv4.conf.all.rp_filter = 1'			>>  /etc/sysctl.conf
	echo 'net.ipv4.conf.default.rp_filter = 1'		>>  /etc/sysctl.conf
	echo 'net.ipv4.tcp_timestamps = 0'			>>  /etc/sysctl.conf
}
Using_TCP_Wrappers()
{
	echo 'Using TCP Wrappers'
	echo "sshd:ALL" >> /etc/hosts.deny
	#echo "sshd:ALL" >> /etc/hosts.allow
}
Disable_Ping_Response()
{
	echo "| Disable_Ping_Response"

	echo 1 > /proc/sys/net/ipv4/icmp_echo_ignore_all
	
	# add the command to /etc/rc.d/rc.local
	filepath="/etc/rc.d/rc.local"
	if [ -f "$filepath" ]
	then
		echo "$filepath found."
	else
		echo "$filepath not found."
		touch $filepath
	fi
	echo "echo 1 > /proc/sys/net/ipv4/icmp_echo_ignore_all" >> $filepath
}
########################################################
echo '';echo '';echo ''
echo '-------------------------------------------'
echo 'OS Protection'
echo '-------------------------------------------'

echo '';echo '';echo ''
#Modifying_fstab;
Package_installs;
single_user_mode;
#protect_grub;
Restricting_Root;
Password_Policies;
password_protection_sha512;
Umask_restrictions;
Reaping_idle_users;
Restricting_cron_and_at;
Sysctl_Security;
Using_TCP_Wrappers;
Disable_Ping_Response;
