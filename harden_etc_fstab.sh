cd /home
echo " create a 32GB file 'mntTmp'"
dd if=/dev/zero of=mntTmp bs=1024 count=32000000

echo "Make an extended filesystem for this file."
mkfs.ext4  /home/mntTmp

echo " Back up your current /tmp directory."
cp -Rpf /tmp /tmp_backup1

cd /

echo " Create the /tmp mounting option to run at boot by using a text editor.
# Add the following to the bottom of the fstab file on a separate line. Then press enter to ensure there is an empty line beneath it (the empty line is important to avoid running into problems when rebooting).
#/home/mntTmp   /tmp    ext4    loop,nosuid,noexec,nodev,rw 0 0<Enter>
# Note: This mount may need to be temporarily removed when you compile or install software
# Look for the line in the fstab file with tmpfs and /shm. Replace 'defaults' with 'defaults,nosuid,noexec,nodev'. Save the file.
"
gedit /etc/fstab

echo " You can now mount the /tmp file system."
mount -o loop,nosuid,noexec,nodev /home/mntTmp /tmp

echo " Set read, write, execute permissions."
chmod 777 /tmp

echo "Check for any mounting errors with the new boot settings."
mount -o remount /tmp


echo " Move the /tmp backup which you created back to the mounted /tmp file system."
mv /tmp_backup1/* /tmp/

echo " Remove the backup that you created."
rm -Rf /tmp_backup1

echo " Backup up /var/tmp."
cp -Rpf var/tmp /tmp_backup2

echo " Remove the /var/tmp directory."
rm -Rf /var/tmp

echo " Create a symbolic link from /var/tmp to /tmp."
ln -s /tmp /var/tmp

echo " Copy the /var/tmp backup to /tmp."
mv /tmp_backup2/* /tmp/

echo " Remove the backup."
rm -Rf /tmp_backup2


'
[test00@localhost Documents]$ su
Password: 
[root@localhost Documents]# bash ./harden_etc_fstab.sh
32000000+0 records in
32000000+0 records out
32768000000 bytes (33 GB) copied, 247.279 s, 133 MB/s
mke2fs 1.42.9 (28-Dec-2013)
/home/mntTmp is not a block special device.
Proceed anyway? (y,n) y
Discarding device blocks: done                            
Filesystem label=
OS type: Linux
Block size=4096 (log=2)
Fragment size=4096 (log=2)
Stride=0 blocks, Stripe width=0 blocks
2003120 inodes, 8000000 blocks
400000 blocks (5.00%) reserved for the super user
First data block=0
Maximum filesystem blocks=2155872256
245 block groups
32768 blocks per group, 32768 fragments per group
8176 inodes per group
Superblock backups stored on blocks: 
	32768, 98304, 163840, 229376, 294912, 819200, 884736, 1605632, 2654208, 
	4096000, 7962624

Allocating group tables: done                            
Writing inode tables: done                            
Creating journal (32768 blocks): done
Writing superblocks and filesystem accounting information: done   


(gedit:18199): dconf-WARNING **: failed to commit changes to dconf: The connection is closed

(gedit:18199): dconf-WARNING **: failed to commit changes to dconf: The connection is closed
Error creating proxy: The connection is closed (g-io-error-quark, 18)
Error creating proxy: The connection is closed (g-io-error-quark, 18)
Error creating proxy: The connection is closed (g-io-error-quark, 18)
Error creating proxy: The connection is closed (g-io-error-quark, 18)
Error creating proxy: The connection is closed (g-io-error-quark, 18)

(gedit:18199): dconf-WARNING **: failed to commit changes to dconf: The connection is closed

(gedit:18199): dconf-WARNING **: failed to commit changes to dconf: The connection is closed

(gedit:18199): dconf-WARNING **: failed to commit changes to dconf: The connection is closed

** (gedit:18199): WARNING **: Set document metadata failed: Setting attribute metadata::gedit-spell-language not supported

** (gedit:18199): WARNING **: Set document metadata failed: Setting attribute metadata::gedit-encoding not supported

** (gedit:18199): WARNING **: Set document metadata failed: Setting attribute metadata::gedit-position not supported

(gedit:18199): dconf-WARNING **: failed to commit changes to dconf: The connection is closed
rm: cannot remove ‘/var/tmp’: Device or resource busy

'

