# centos7_hardening2.sh
#
# This CentOS7 hardening script is implemented with this guide:
# https://highon.coffee/blog/security-harden-centos-7/
#

timestamp="`date +%Y-%m-%d_%H-%M-%S`"

# backup this script
#cp -p /home/user0/Documents/centos7_hardening2.sh /run/media/user0/HDDREG/tools/centos7_hardening2.sh

backupFile()
{
	originalFilePath="$1";

	if [ -f "$originalFilePath" ]
	then
		dir=`dirname $originalFilePath`;
		base=`basename $originalFilePath`;
		# cp command can't copy the file to the source directory, 
		# so I copy the source file to /tmp and then move it back with timestamp in 
		# file name  to source directory.
		# copy the file to /tmp
		cp -p $originalFilePath /tmp;

		# move the /tmp/$file back to src dir
		mv -f "/tmp/$base" "$originalFilePath-$timestamp";	
	else
		echo "$originalFilePath not found."
	fi
}

On_NTP()
{
	echo '|Install NTP'


	yum install ntp ntpdate
	chkconfig ntpd on
	ntpdate pool.ntp.org
	/etc/init.d/ntpd start

	echo 'Specify Additional Remote NTP Servers'
	backupFile /etc/ntp.conf;
	echo "server ntpserver" >> /etc/ntp.conf
}

Configure_System_for_AIDE()
{
	echo '|Configure System for AIDE'
	# Disable prelinking altogether
	#
	backupFile /etc/sysconfig/prelink
	if grep -q ^PRELINKING /etc/sysconfig/prelink
	then
	  sed -i 's/PRELINKING.*/PRELINKING=no/g' /etc/sysconfig/prelink
	else
	  echo -e "\n# Set PRELINKING=no per security requirements" >> /etc/sysconfig/prelink
	  echo "PRELINKING=no" >> /etc/sysconfig/prelink
	fi
	# Disable previous prelink changes to binaries
	/usr/sbin/prelink -ua
	
	#
	echo ''
	echo ''
	echo ''
	echo '|Install AIDE'
	yum install aide -y && /usr/sbin/aide --init && cp /var/lib/aide/aide.db.new.gz /var/lib/aide/aide.db.gz && /usr/sbin/aide --check
	echo '|Configure periodic execution of AIDE, runs every morning at 20:30'
	echo "30 20 * * * /usr/sbin/aide --check" >> /etc/crontab
}

Enable_Secure_high_quality_Password_Policy()
{
	echo '|Enable Secure (high quality) Password Policy'
	authconfig --passalgo=sha512 --update
}

Verify_grub_Permissions()
{
	echo '|Verify /boot/grub2/grub.cfg Permissions'
	backupFile /boot/grub2/grub.cfg
	chmod 600 /boot/grub2/grub.cfg
}

Require_Authentication_for_Single_User_Mode()
{
	echo '|Require Authentication for Single User Mode'
	backupFile /etc/sysconfig/init
	echo "SINGLE=/sbin/sulogin" >> /etc/sysconfig/init
}
Disable_Ctrl_Alt_Del_Reboot_Activation()
{
	echo '|Disable Ctrl-Alt-Del Reboot Activation'
	backupFile /etc/init/control-alt-delete.conf
	echo 'Change:'
	echo 'exec /sbin/shutdown -r now "Control-Alt-Delete pressed"'
	echo 'To:'
	echo 'exec /usr/bin/logger -p security.info "Control-Alt-Delete pressed"'
	gedit /etc/init/control-alt-delete.conf
}
Disable_Zeroconf_Networking()
{
	echo '|Disable Zeroconf Networking'
	backupFile /etc/sysconfig/network
	echo "NOZEROCONF=yes" >> /etc/sysconfig/network
}
DisableIPv6()
{
	echo '|Disable IPv6 Support Automatically Loading'

	# file not found
	#backupFile /etc/modprobe.d/disabled.conf
	#echo 'options ipv6 disable=1' >> /etc/modprobe.d/disabled.conf
	
	backupFile /etc/sysconfig/network
	echo 'NETWORKING_IPV6=no'>> /etc/sysconfig/network
	echo 'IPV6INIT=no'>> /etc/sysconfig/network
}
Securing_root_Logins()
{
	echo '|Securing root Logins'
	backupFile /etc/securetty
	echo "tty1" > /etc/securetty
	#echo "console" > /etc/securetty
	chmod 700 /root
}

Enable_UMASK_077()
{
	echo '|Enable UMASK 077'
	perl -npe 's/umask\s+0\d2/umask 077/g' -i /etc/bashrc
	perl -npe 's/umask\s+0\d2/umask 077/g' -i /etc/csh.cshrc
}

Prune_Idle_Users()
{
	echo '|Prune Idle Users'
	backupFile /etc/profile.d/os-security.sh

	echo "Idle users will be removed after 15 minutes"
	echo "readonly TMOUT=900" >> /etc/profile.d/os-security.sh
	echo "readonly HISTFILE"  >> /etc/profile.d/os-security.sh
	chmod +x /etc/profile.d/os-security.sh
}

Securing_Cron()
{
	echo '|Securing Cron'

	echo "Locking down Cron"
	touch /etc/cron.allow
	chmod 600 /etc/cron.allow
	awk -F: '{print $1}' /etc/passwd | grep -v root > /etc/cron.deny
	echo "Locking down AT"
	touch /etc/at.allow
	chmod 600 /etc/at.allow
	awk -F: '{print $1}' /etc/passwd | grep -v root > /etc/at.deny
}

Sysctl_Security()
{
	backupFile /etc/sysctl.conf

	echo '|Sysctl Security'
	echo "# my data" 					>> /etc/sysctl.conf
	echo "net.ipv4.ip_forward = 0" 				>> /etc/sysctl.conf
	echo "net.ipv4.conf.all.send_redirects = 0" 		>> /etc/sysctl.conf
	echo "net.ipv4.conf.all.accept_source_route = 0" 	>> /etc/sysctl.conf
	echo "net.ipv4.conf.all.accept_redirects = 0" 		>> /etc/sysctl.conf
	echo "net.ipv4.conf.all.secure_redirects = 0" 		>> /etc/sysctl.conf
	echo "net.ipv4.conf.all.log_martians = 1" 		>> /etc/sysctl.conf
	echo "net.ipv4.conf.default.send_redirects = 0" 	>> /etc/sysctl.conf
	echo "net.ipv4.conf.default.accept_source_route = 0" 	>> /etc/sysctl.conf
	echo "net.ipv4.conf.default.accept_redirects = 0" 	>> /etc/sysctl.conf
	echo "net.ipv4.conf.default.secure_redirects = 0" 	>> /etc/sysctl.conf
	echo "net.ipv4.icmp_echo_ignore_broadcasts = 1" 	>> /etc/sysctl.conf
	echo "net.ipv4.icmp_ignore_bogus_error_responses = 1" 	>> /etc/sysctl.conf
	echo "net.ipv4.tcp_syncookies = 1" 			>> /etc/sysctl.conf
	echo "net.ipv4.conf.all.rp_filter = 1" 			>> /etc/sysctl.conf
	echo "net.ipv4.conf.default.rp_filter = 1" 		>> /etc/sysctl.conf
	echo "net.ipv4.tcp_timestamps = 0" 			>> /etc/sysctl.conf
	echo "net.ipv4.tcp_max_syn_backlog = 1280" 		>> /etc/sysctl.conf

	# lynis warnings
	#echo "kernel.kptr_restrict = 1" 			>> /etc/sysctl.conf
	#echo "kernel.sysrq = 0" 				>> /etc/sysctl.conf
	#echo "net.ipv4.conf.all.forwarding = 0" 		>> /etc/sysctl.conf
	#echo "net.ipv4.conf.default.log_martians = 1" 		>> /etc/sysctl.conf
	#echo "net.ipv6.conf.all.accept_redirects = 0" 		>> /etc/sysctl.conf
	#echo "net.ipv6.conf.default.accept_redirects = 0" 	>> /etc/sysctl.conf

	echo 'Disable ping response'
	echo "net.ipv4.conf.icmp_echo_ignore_all = 1" 		>> /etc/sysctl.conf




	# Buffer_Overflow_Protection
	echo "Enable ExecShield. Helps prevent stack smashing / BOF"
	sysctl -w kernel.exec-shield=1
	echo "kernel.exec-shield = 1" >> /etc/sysctl.conf

	echo "Check / Enable ASLR"
	sysctl -q -n -w kernel.randomize_va_space=2
	echo "kernel.randomize_va_space = 2" >> /etc/sysctl.conf
	
	echo "---------------------------------------------------------"
	echo "Check BIOS>Power and ensure XD(Intel)/NX(AMD) is enabled."
	echo "---------------------------------------------------------"
}

Deny_All_TCP_Wrappers()
{
	echo "|Deny All TCP Wrappers"
	backupFile  /etc/hosts.deny
	backupFile  /etc/hosts.allow
	echo "ALL:ALL"  >> /etc/hosts.deny
	#echo "sshd:ALL" >> /etc/hosts.allow
}

Verify_iptables_Enabled()
{
	echo "|Verify iptables Enabled"
	systemctl enable iptables
	systemctl start iptables.service
}

Disable_Uncommon_Protocols()
{
	echo "|Disable Uncommon Protocols"
	backupFile /etc/modprobe.d/dccp.conf
	backupFile /etc/modprobe.d/sctp.conf
	backupFile /etc/modprobe.d/rds.conf
	backupFile /etc/modprobe.d/tipc.conf
	echo "install dccp /bin/false" > /etc/modprobe.d/dccp.conf
	echo "install sctp /bin/false" > /etc/modprobe.d/sctp.conf
	echo "install rds /bin/false"  > /etc/modprobe.d/rds.conf
	echo "install tipc /bin/false" > /etc/modprobe.d/tipc.conf
}

Enable_Rsyslog()
{
	echo "|Ensure Rsyslog is installed"
	yum -y install rsyslog
	echo "|Enable Rsyslog"
	systemctl enable rsyslog.service
	systemctl start rsyslog.service
}

On_Auditd()
{
	echo '|Enable auditd Service'
	systemctl enable auditd.service
	systemctl start auditd.service

	# But /etc/grub.conf doesn't exist on my centos7
	#echo "kernel /vmlinuz-version ro vga=ext root=/dev/VolGroup00/LogVol00 rhgb quiet audit=1" >> /etc/grub.conf
	
	backupFile /etc/audit/auditd.conf
	echo "modify /etc/audit/auditd.conf"
	echo "num_logs = 5"
	echo "max_log_file = 30MB"            # default 6
	echo "max_log_file_action = rotate"
	echo "space_left_action = email"      # default SYSLOG
	echo "admin_space_left_action = halt" # default SUSPEND
	echo "action_mail_acct = root"        # add this line
	gedit /etc/audit/auditd.conf

	backupFile /etc/audisp/plugins.d/syslog.conf
	echo ""
	echo ""
	echo ""
	echo "| active = yes" # default no
	gedit /etc/audisp/plugins.d/syslog.conf
	service auditd restart

	backupFile /etc/audit/audit.rules
	echo "" >> /etc/audit/audit.rules
	echo "# audit_time_rules - Record attempts to alter time through adjtime" >> /etc/audit/audit.rules
	echo "-a always,exit -F arch=b64 -S adjtimex -k audit_time_rules" >> /etc/audit/audit.rules

	echo "# audit_time_rules - Record attempts to alter time through settimeofday" >> /etc/audit/audit.rules
	echo "-a always,exit -F arch=b64 -S settimeofday -k audit_time_rules" >> /etc/audit/audit.rules

	echo "# audit_time_rules - Record Attempts to Alter Time Through stime" >> /etc/audit/audit.rules
	echo "-a always,exit -F arch=b64 -S adjtimex -S settimeofday -S clock_settime" >> /etc/audit/audit.rules
	echo "-k audit_time_rules" >> /etc/audit/audit.rules

	echo "# audit_time_rules - Record Attempts to Alter Time Through clock_settime" >> /etc/audit/audit.rules
	echo "-a always,exit -F arch=b64 -S clock_settime -k audit_time_rules" >> /etc/audit/audit.rules

	echo "# Record Attempts to Alter the localtime File" >> /etc/audit/audit.rules
	echo "-w /etc/localtime -p wa -k audit_time_rules" >> /etc/audit/audit.rules

	echo "# Record Events that Modify User/Group Information" >> /etc/audit/audit.rules
	echo "# audit_account_changes" >> /etc/audit/audit.rules
	echo "-w /etc/group -p wa -k audit_account_changes" >> /etc/audit/audit.rules
	echo "-w /etc/passwd -p wa -k audit_account_changes" >> /etc/audit/audit.rules
	echo "-w /etc/gshadow -p wa -k audit_account_changes" >> /etc/audit/audit.rules
	echo "-w /etc/shadow -p wa -k audit_account_changes" >> /etc/audit/audit.rules
	echo "-w /etc/security/opasswd -p wa -k audit_account_changes" >> /etc/audit/audit.rules

	echo "# Record Events that Modify the System's Network Environment" >> /etc/audit/audit.rules
	echo "# audit_network_modifications" >> /etc/audit/audit.rules
	echo "-a always,exit -F arch=ARCH -S sethostname -S setdomainname -k audit_network_modifications" >> /etc/audit/audit.rules
	echo "-w /etc/issue -p wa -k audit_network_modifications" >> /etc/audit/audit.rules
	echo "-w /etc/issue.net -p wa -k audit_network_modifications" >> /etc/audit/audit.rules
	echo "-w /etc/hosts -p wa -k audit_network_modifications" >> /etc/audit/audit.rules
	echo "-w /etc/sysconfig/network -p wa -k audit_network_modifications" >> /etc/audit/audit.rules

	echo "#Record Events that Modify the System's Mandatory Access Controls" >> /etc/audit/audit.rules
	echo "-w /etc/selinux/ -p wa -k MAC-policy" >> /etc/audit/audit.rules

	echo "#Record Events that Modify the System's Discretionary Access Controls - chmod" >> /etc/audit/audit.rules
	echo "-a always,exit -F arch=b32 -S chmod -F auid>=500 -F auid!=4294967295 -k perm_mod" >> /etc/audit/audit.rules
	echo "-a always,exit -F arch=b64 -S chmod  -F auid>=500 -F auid!=4294967295 -k perm_mod" >> /etc/audit/audit.rules

	echo "#Record Events that Modify the System's Discretionary Access Controls - chown" >> /etc/audit/audit.rules
	echo "-a always,exit -F arch=b32 -S chown -F auid>=500 -F auid!=4294967295 -k perm_mod" >> /etc/audit/audit.rules
	echo "-a always,exit -F arch=b64 -S chown -F auid>=500 -F auid!=4294967295 -k perm_mod" >> /etc/audit/audit.rules

	echo "#Record Events that Modify the System's Discretionary Access Controls - fchmod" >> /etc/audit/audit.rules
	echo "-a always,exit -F arch=b32 -S fchmod -F auid>=500 -F auid!=4294967295 -k perm_mod" >> /etc/audit/audit.rules
	echo "-a always,exit -F arch=b64 -S fchmod -F auid>=500 -F auid!=4294967295 -k perm_mod" >> /etc/audit/audit.rules

	echo "#Record Events that Modify the System's Discretionary Access Controls - fchmodat" >> /etc/audit/audit.rules
	echo "-a always,exit -F arch=b32 -S fchmodat -F auid>=500 -F auid!=4294967295 -k perm_mod" >> /etc/audit/audit.rules
	echo "-a always,exit -F arch=b64 -S fchmodat -F auid>=500 -F auid!=4294967295 -k perm_mod" >> /etc/audit/audit.rules

	echo "#Record Events that Modify the System's Discretionary Access Controls - fchown" >> /etc/audit/audit.rules
	echo "-a always,exit -F arch=b32 -S fchown -F auid>=500 -F auid!=4294967295 -k perm_mod" >> /etc/audit/audit.rules
	echo "-a always,exit -F arch=b64 -S fchown -F auid>=500 -F auid!=4294967295 -k perm_mod" >> /etc/audit/audit.rules

	echo "#Record Events that Modify the System's Discretionary Access Controls - fchownat" >> /etc/audit/audit.rules
	echo "-a always,exit -F arch=b32 -S fchownat -F auid>=500 -F auid!=4294967295 -k perm_mod" >> /etc/audit/audit.rules
	echo "-a always,exit -F arch=b64 -S fchownat -F auid>=500 -F auid!=4294967295 -k perm_mod" >> /etc/audit/audit.rules

	echo "#Record Events that Modify the System's Discretionary Access Controls - fremovexattr" >> /etc/audit/audit.rules
	echo "-a always,exit -F arch=b32 -S fremovexattr -F auid>=500 -F auid!=4294967295 -k perm_mod" >> /etc/audit/audit.rules
	echo "-a always,exit -F arch=b64 -S fremovexattr -F auid>=500 -F auid!=4294967295 -k perm_mod" >> /etc/audit/audit.rules

	echo "#Record Events that Modify the System's Discretionary Access Controls - fsetxattr" >> /etc/audit/audit.rules
	echo "-a always,exit -F arch=b32 -S fsetxattr -F auid>=500 -F auid!=4294967295 -k perm_mod" >> /etc/audit/audit.rules
	echo "-a always,exit -F arch=b64 -S fsetxattr -F auid>=500 -F auid!=4294967295 -k perm_mod" >> /etc/audit/audit.rules

	echo "#Record Events that Modify the System's Discretionary Access Controls - lchown" >> /etc/audit/audit.rules
	echo "-a always,exit -F arch=b32 -S lchown -F auid>=500 -F auid!=4294967295 -k perm_mod" >> /etc/audit/audit.rules
	echo "-a always,exit -F arch=b64 -S lchown -F auid>=500 -F auid!=4294967295 -k perm_mod" >> /etc/audit/audit.rules

	echo "#Record Events that Modify the System's Discretionary Access Controls - lremovexattr" >> /etc/audit/audit.rules
	echo "-a always,exit -F arch=b32 -S lremovexattr -F auid>=500 -F auid!=4294967295 -k perm_mod" >> /etc/audit/audit.rules
	echo "-a always,exit -F arch=b64 -S lremovexattr -F auid>=500 -F auid!=4294967295 -k perm_mod" >> /etc/audit/audit.rules

	echo "#Record Events that Modify the System's Discretionary Access Controls - lsetxattr" >> /etc/audit/audit.rules
	echo "-a always,exit -F arch=b32 -S lsetxattr -F auid>=500 -F auid!=4294967295 -k perm_mod" >> /etc/audit/audit.rules
	echo "-a always,exit -F arch=b64 -S lsetxattr -F auid>=500 -F auid!=4294967295 -k perm_mod" >> /etc/audit/audit.rules

	echo "#Record Events that Modify the System's Discretionary Access Controls - removexattr" >> /etc/audit/audit.rules
	echo "-a always,exit -F arch=b32 -S removexattr -F auid>=500 -F auid!=4294967295 -k perm_mod" >> /etc/audit/audit.rules
	echo "-a always,exit -F arch=b64 -S removexattr -F auid>=500 -F auid!=4294967295 -k perm_mod-a always,exit -F arch=b32 -S fchmodat -F auid>=500 -F auid!=4294967295 -k perm_mod" >> /etc/audit/audit.rules
	echo "-a always,exit -F arch=b64 -S fchmodat -F auid>=500 -F auid!=4294967295 -k perm_mod" >> /etc/audit/audit.rules

	echo "#Record Events that Modify the System's Discretionary Access Controls - fchown" >> /etc/audit/audit.rules
	echo "-a always,exit -F arch=b32 -S fchown -F auid>=500 -F auid!=4294967295 -k perm_mod" >> /etc/audit/audit.rules
	echo "-a always,exit -F arch=b64 -S fchown -F auid>=500 -F auid!=4294967295 -k perm_mod" >> /etc/audit/audit.rules

	echo "#Record Events that Modify the System's Discretionary Access Controls - fchownat" >> /etc/audit/audit.rules
	echo "-a always,exit -F arch=b32 -S fchownat -F auid>=500 -F auid!=4294967295 -k perm_mod" >> /etc/audit/audit.rules
	echo "-a always,exit -F arch=b64 -S fchownat -F auid>=500 -F auid!=4294967295 -k perm_mod" >> /etc/audit/audit.rules

	echo "#Record Events that Modify the System's Discretionary Access Controls - fremovexattr" >> /etc/audit/audit.rules
	echo "-a always,exit -F arch=b32 -S fremovexattr -F auid>=500 -F auid!=4294967295 -k perm_mod" >> /etc/audit/audit.rules
	echo "-a always,exit -F arch=b64 -S fremovexattr -F auid>=500 -F auid!=4294967295 -k perm_mod" >> /etc/audit/audit.rules

	echo "#Record Events that Modify the System's Discretionary Access Controls - fsetxattr" >> /etc/audit/audit.rules
	echo "-a always,exit -F arch=b32 -S lsetxattr -F auid>=500 -F auid!=4294967295 -k perm_mod" >> /etc/audit/audit.rules
	echo "-a always,exit -F arch=b64 -S lsetxattr -F auid>=500 -F auid!=4294967295 -k perm_mod" >> /etc/audit/audit.rules

	echo "#Record Events that Modify the System's Discretionary Access Controls - removexattr" >> /etc/audit/audit.rules
	echo "-a always,exit -F arch=b32 -S removexattr -F auid>=500 -F auid!=4294967295 -k perm_mod" >> /etc/audit/audit.rules
	echo "-a always,exit -F arch=b64 -S removexattr -F auid>=500 -F auid!=4294967295 -k perm_mod" >> /etc/audit/audit.rules

	echo "#Record Events that Modify the System's Discretionary Access Controls - setxattr" >> /etc/audit/audit.rules
	echo "-a always,exit -F arch=b32 -S setxattr -F auid>=500 -F auid!=4294967295 -k perm_mod" >> /etc/audit/audit.rules
	echo "-a always,exit -F arch=b64 -S setxattr -F auid>=500 -F auid!=4294967295 -k perm_mod" >> /etc/audit/audit.rules

	echo "#Record Attempts to Alter Logon and Logout Events" >> /etc/audit/audit.rules
	echo "-w /var/log/faillog -p wa -k logins" >> /etc/audit/audit.rules
	echo "-w /var/log/lastlog -p wa -k logins" >> /etc/audit/audit.rules

	echo "#Record Attempts to Alter Process and Session Initiation Information" >> /etc/audit/audit.rules
	echo "-w /var/run/utmp -p wa -k session" >> /etc/audit/audit.rules
	echo "-w /var/log/btmp -p wa -k session" >> /etc/audit/audit.rules
	echo "-w /var/log/wtmp -p wa -k session" >> /etc/audit/audit.rules

	echo "#Ensure auditd Collects Unauthorized Access Attempts to Files (unsuccessful)" >> /etc/audit/audit.rules
	echo "-a always,exit -F arch=b32 -S creat -S open -S openat -S open_by_handle_at -S truncate -S ftruncate -F exit=-EACCES -F auid>=500 -F auid!=4294967295 -k access" >> /etc/audit/audit.rules
	echo "-a always,exit -F arch=b32 -S creat -S open -S openat -S open_by_handle_at -S truncate -S ftruncate -F exit=-EPERM -F auid>=500 -F auid!=4294967295 -k access" >> /etc/audit/audit.rules
	echo "-a always,exit -F arch=b64 -S creat -S open -S openat -S open_by_handle_at -S truncate -S ftruncate -F exit=-EACCES -F auid>=500 -F auid!=4294967295 -k access" >> /etc/audit/audit.rules
	echo "-a always,exit -F arch=b64 -S creat -S open -S openat -S open_by_handle_at -S truncate -S ftruncate -F exit=-EPERM -F auid>=500 -F auid!=4294967295 -k access" >> /etc/audit/audit.rules

	echo "#Ensure auditd Collects Information on the Use of Privileged Commands" >> /etc/audit/audit.rules
	echo "#" >> /etc/audit/audit.rules
	echo "#  Find setuid / setgid programs then modify and uncomment the line below." >> /etc/audit/audit.rules
	echo "#" >> /etc/audit/audit.rules
	echo "##  sudo find / -xdev -type f -perm -4000 -o -perm -2000 2>/dev/null" >> /etc/audit/audit.rules
	echo "#" >> /etc/audit/audit.rules
	echo "# -a always,exit -F path=SETUID_PROG_PATH -F perm=x -F auid>=500 -F auid!=4294967295 -k privileged" >> /etc/audit/audit.rules

	echo "#Ensure auditd Collects Information on Exporting to Media (successful)" >> /etc/audit/audit.rules
	echo "-a always,exit -F arch=ARCH -S mount -F auid>=500 -F auid!=4294967295 -k export" >> /etc/audit/audit.rules

	echo "#Ensure auditd Collects File Deletion Events by User" >> /etc/audit/audit.rules
	echo "-a always,exit -F arch=ARCH -S rmdir -S unlink -S unlinkat -S rename -S renameat -F auid>=500 -F auid!=4294967295 -k delete" >> /etc/audit/audit.rules

	echo "#Ensure auditd Collects System Administrator Actions" >> /etc/audit/audit.rules
	echo "-w /etc/sudoers -p wa -k actions" >> /etc/audit/audit.rules

	echo "#Ensure auditd Collects Information on Kernel Module Loading and Unloading" >> /etc/audit/audit.rules
	echo "-w /sbin/insmod -p x -k modules" >> /etc/audit/audit.rules
	echo "-w /sbin/rmmod -p x -k modules" >> /etc/audit/audit.rules
	echo "-w /sbin/modprobe -p x -k modules" >> /etc/audit/audit.rules
	echo "-a always,exit -F arch=b64 -S init_module -S delete_module -k modules" >> /etc/audit/audit.rules

	echo "#Make the auditd Configuration Immutable" >> /etc/audit/audit.rules
	echo "-e 2" >> /etc/audit/audit.rules
}

Remove_Services()
{
	echo "| Bulk Remove of Services"
	# Remove
	yum remove xinetd
	yum remove telnet-server
	yum remove rsh-server
	yum remove telnet
	yum remove rsh-server
	yum remove rsh
	yum remove ypbind
	yum remove ypserv
	yum remove tftp-server
	yum remove cronie-anacron
	yum remove bind
	yum remove vsftpd
	yum remove httpd
	yum remove dovecot
	yum remove squid
	yum remove net-snmpd
	
	echo "| Bulk Enable / Disable Services"
	#Disable / Enable
	systemctl disable xinetd #Failed to execute operation: Access denied
	systemctl disable rexec  #Failed to execute operation: Access denied
	systemctl disable rsh    #Failed to execute operation: Access denied
	systemctl disable rlogin #Failed to execute operation: Access denied
	systemctl disable ypbind #Failed to execute operation: Access denied
	systemctl disable tftp   #Failed to execute operation: Access denied
	systemctl disable certmonger
	systemctl disable cgconfig
	systemctl disable cgred
	systemctl disable cpuspeed #Failed to execute operation: Access denied
	systemctl enable irqbalance
	systemctl disable kdump
	systemctl disable mdmonitor
	systemctl disable messagebus
	systemctl disable netconsole #netconsole.service is not a native service, redirecting to /sbin/chkconfig.Executing /sbin/chkconfig netconsole off
	systemctl disable ntpdate
	systemctl disable oddjobd
	systemctl disable portreserve #Failed to execute operation: Access denied
	systemctl enable psacct
	systemctl disable qpidd #Failed to execute operation: Access denied
	systemctl disable quota_nld #Failed to execute operation: Access denied
	systemctl disable rdisc
	systemctl disable rhnsd #Failed to execute operation: Access denied
	systemctl disable rhsmcertd #Failed to execute operation: Access denied
	systemctl disable saslauthd
	systemctl disable smartd
	systemctl disable sysstat #Failed to execute operation: Access denied
	systemctl enable crond
	systemctl disable atd
	systemctl disable nfslock
	systemctl disable named #Failed to execute operation: Access denied
	systemctl disable httpd #Failed to execute operation: Access denied
	systemctl disable dovecot #Failed to execute operation: Access denied
	systemctl disable squid #Failed to execute operation: Access denied
	systemctl disable snmpd #Failed to execute operation: Access denied

	echo "| Disable Secure RPC Client Service"
	systemctl disable rpcgssd

	echo "| Disable Secure RPC Server Service"
	systemctl disable rpcsvcgssd

	echo "| Disable RPC ID Mapping Service"
	systemctl disable rpcidmapd
	systemctl disable netfs #Failed to execute operation: Access denied

	echo "| Disable Network File System (nfs)"
	systemctl disable nfs

	#echo 'If you don’t need SSH disable it'
	#systemctl disable sshd
	#Disable SSH iptables Firewall rule
	#-A INPUT -m state --state NEW -m tcp -p tcp --dport 22 -j ACCEPT

	echo "| Remove Rsh Trust Files"
	backupFile /etc/hosts.equiv
	backupFile ~/.rhosts
	rm /etc/hosts.equiv
	rm ~/.rhosts

	echo "| Disable Avahi Server Software"
	systemctl disable avahi-daemon

	echo "| Disable the CUPS Service"
	systemctl disable cups

}

On_DHCP()
{
	echo "| Disable DHCP Service"
	systemctl disable dhcpd

	echo "| Uninstall DHCP Server Package"
	yum erase dhcp

	echo "| Disable DHCP Client"

echo "Open /etc/sysconfig/network-scripts/ifcfg-eth0 (if you have more interfaces, do this for each one) and make sure the address is statically assigned with the BOOTPROTO=none

Example:
BOOTPROTO=none
NETMASK=255.255.255.0
IPADDR=192.168.1.2
GATEWAY=192.168.1.1"

}

On_Postfix()
{
	echo 'Enable Postfix'
	systemctl enable postfix
	echo 'Remove Sendmail'
	yum remove sendmail

	#Postfix Disable Network Listening
	backupFile /etc/postfix/main.cf
	echo "|/etc/postfix/main.cf. ensure the following inet_interfaces line appears:inet_interfaces = localhost"
	gedit  /etc/postfix/main.cf
}
On_Audit_logs()
{
	echo '| Audit Logs'
	echo 'system audit logs must have 0640 or less permissions set.'
	chmod 0640 audit_file
	echo 'System Audit Logs Must Be Owned By Root'
	chown root/var/log
}
Disable_autofs()
{
	echo "| Disable autofs"
	chkconfig --level 0123456 autofs off
	service autofs stop
}

Disable_uncommon_filesystems()
{
	echo "| Disable uncommon filesystems"
	backupFile /etc/modprobe.d/cramfs.conf
	backupFile /etc/modprobe.d/freevxfs.conf
	backupFile /etc/modprobe.d/jffs2.conf
	backupFile /etc/modprobe.d/hfs.conf
	backupFile /etc/modprobe.d/hfsplus.conf
	backupFile /etc/modprobe.d/squashfs.conf
	backupFile /etc/modprobe.d/udf.conf
	echo "install cramfs /bin/false"   > /etc/modprobe.d/cramfs.conf
	echo "install freevxfs /bin/false" > /etc/modprobe.d/freevxfs.conf
	echo "install jffs2 /bin/false"    > /etc/modprobe.d/jffs2.conf
	echo "install hfs /bin/false"      > /etc/modprobe.d/hfs.conf
	echo "install hfsplus /bin/false"  > /etc/modprobe.d/hfsplus.conf
	echo "install squashfs /bin/false" > /etc/modprobe.d/squashfs.conf
	echo "install udf /bin/false"      > /etc/modprobe.d/udf.conf
}

On_core_dumps()
{
	echo "| Disable core dumps for all users"
	backupFile /etc/security/limits.conf
	# 禁止创建core文件
	echo "* hard core 0"   >> /etc/security/limits.conf
	# 除root外，其他用户最多使用5M内存
	echo "* hard rss 5000" >> /etc/security/limits.conf
	# 最多进程数限制为20
	echo "* hard nproc 20" >> /etc/security/limits.conf

	echo "| Disable core dumps for SUID programs"
	# Set runtime for fs.suid_dumpable
	#
	sysctl -q -n -w fs.suid_dumpable=0


	#
	# If fs.suid_dumpable present in /etc/sysctl.conf, change value to "0"
	#     else, add "fs.suid_dumpable = 0" to /etc/sysctl.conf
	#
	backupFile /etc/sysctl.conf
	if grep --silent ^fs.suid_dumpable /etc/sysctl.conf ; then
	     sed -i 's/^fs.suid_dumpable.*/fs.suid_dumpable = 0/g' /etc/sysctl.conf
	else
	     echo "" >> /etc/sysctl.conf
	     echo "# Set fs.suid_dumpable to 0 per security requirements" >> /etc/sysctl.conf
	     echo "fs.suid_dumpable = 0" >> /etc/sysctl.conf
	fi

}


On_SELinux()
{
	backupFile /etc/grub.conf
	echo "| Confirm SELinux is not disabled"
	sed -i "s/selinux=0//gI"   /etc/grub.conf
	sed -i "s/enforcing=0//gI" /etc/grub.conf


	backupFile /etc/selinux/config
	echo "| Open /etc/selinux/config and check for SELINUXTYPE=targeted or SELINUXTYPE=enforcing, depending on your requirements."
	gedit /etc/selinux/config


	echo "| Enable the SELinux restorecond Service"
	echo "| Enable restorecond for all run levels:"
	chkconfig --level 0123456 restorecond on
	echo "| Start restorecond if not currently running:"
	service restorecond start


	echo "| Check no daemons are unconfined by SELinux"
	sudo ps -eZ | egrep "initrc" | egrep -vw "tr|ps|egrep|bash|awk" | tr ':' ' ' | awk '{ print $NF }'
	echo "| This should return no output."
}

Prevent_Log_Into_Accounts_With_Empty_Password()
{
	backupFile /etc/pam.d/system-auth
	sed -i 's/\<nullok\>//g' /etc/pam.d/system-auth
}
On_SSH()
{
	echo "| # Protocol 2,1 --> Protocol 2"
	backupFile /etc/ssh/ssh_config
	gedit /etc/ssh/ssh_config

	echo "| Allow Only SSH Protocol 2"
	echo "| Open /etc/ssh/sshd_config and ensure the following line exists:Protocol 2"
	echo "| PermitRootLogin no"
	echo "| HostbasedAuthentication no"
	echo "| IgnoreRhosts yes"
	echo "| PermitEmptyPasswords no"
	echo "| PermitUserEnvironment no"
	echo "| ClientAliveInterval 300  #Set SSH Idle Timeout Interval(seconds)"
	echo "| ClientAliveCountMax 0         #指如果发现客户端没有相应，则判断一次超时，这个参数设置允许超时的次数"
	echo "| Banner /etc/issue"
	echo "| DenyUsers USER1 USER2         #Limit Users’ SSH Access"
	
	backupFile /etc/ssh/sshd_config
	gedit /etc/ssh/sshd_config

	echo "restart ssh"
	systemctl restart sshd.service 
}
On_Update()
{
	echo "| Prompt OS update installation"
	yum -y install yum-cron
	chkconfig yum-cron on
}

Passwd_For_SingleUserMode()
{
	echo "| Passwd_For_SingleUserMode"

	backupFile /etc/inittab
	echo "# Require the root pw when booting into single user mode" >> /etc/inittab
	echo "~~:S:wait:/sbin/sulogin" >> /etc/inittab
	echo "Don't allow any nut to kill the server"
	perl -npe 's/ca::ctrlaltdel:\/sbin\/shutdown/#ca::ctrlaltdel:\/sbin\/shutdown/' -i /etc/inittab
}

Install_Packages()
{
	echo "| Install clamav"
	yum install clamav clamav-daemon clamav-freshclam clamav-unofficial-sigs
	freshclam 
	service clamav-daemon start


	echo "| Install lynis"
	yum install lynis


	lynis audit system

	rpm -Uvh lux-release-7-1.noarch.rpm 
	yum install maldetect



}


echo '';echo '';echo ''
echo '-------------------------------------------'
echo 'Security Harden CentOS 7'
echo '-------------------------------------------'

echo '';echo '';echo ''
On_NTP;
#
echo '';echo '';echo ''
Configure_System_for_AIDE;
#
echo '';echo '';echo ''
Enable_Secure_high_quality_Password_Policy;
#
echo '';echo '';echo ''
Verify_grub_Permissions;
#
echo '';echo '';echo ''
Require_Authentication_for_Single_User_Mode;
#
echo '';echo '';echo ''
Disable_Ctrl_Alt_Del_Reboot_Activation;
#
echo '';echo '';echo ''
Disable_Zeroconf_Networking;
#
echo '';echo '';echo ''
DisableIPv6;
#
echo '';echo '';echo ''
Securing_root_Logins;
#
echo '';echo '';echo ''
Enable_UMASK_077;
#
echo '';echo '';echo ''
Prune_Idle_Users;
#
echo '';echo '';echo ''
Securing_Cron;
#
echo '';echo '';echo ''
Sysctl_Security;
#
echo '';echo '';echo ''
Deny_All_TCP_Wrappers;
#
echo '';echo '';echo ''
Verify_iptables_Enabled;
#
echo '';echo '';echo ''
Disable_Uncommon_Protocols;
#
echo '';echo '';echo ''
Enable_Rsyslog;
#
echo '';echo '';echo ''
On_Auditd;
#
echo '';echo '';echo ''
Remove_Services;
#
echo '';echo '';echo ''
On_DHCP;
#
echo '';echo '';echo ''
On_Postfix;
#
echo '';echo '';echo ''
On_Audit_logs;
#
echo '';echo '';echo ''
Disable_autofs;
#
echo '';echo '';echo ''
Disable_uncommon_filesystems;
#
echo '';echo '';echo ''
#On_core_dumps;
#
echo '';echo '';echo ''
On_SELinux;
#
echo '';echo '';echo ''
Prevent_Log_Into_Accounts_With_Empty_Password;
#
echo '';echo '';echo ''
On_SSH;
#
echo '';echo '';echo ''
On_Update;
#
echo '';echo '';echo ''
#Passwd_For_SingleUserMode;
#
echo '';echo '';echo ''
#Install_Packages;


echo '-------------------------------------------'
echo ' '
echo '-------------------------------------------'
echo ''
echo ''
echo ''
echo 'Disable ping response'
echo '/etc/sysctl.conf	set net.ipv4.conf.icmp_echo_ignore_all = 1'
#gedit /etc/sysctl.conf
