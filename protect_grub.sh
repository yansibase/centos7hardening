# source:
# http://www.tecmint.com/things-to-do-after-minimal-rhel-centos-7-installation/6/
# https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/7/html/System_Administrators_Guide/sec-GRUB_2_Password_Protection.html


echo 'backup grub.cfg and 10_linux'
cp /boot/grub2/grub.cfg /boot/grub2/grub.cfg_bak
cp /etc/grub.d/10_linux /etc/grub.d/10_linux_bak



echo "Now open ‘/etc/grub.d/10_linux‘ and add the below line at the end of the file."
echo 'cat <<EOF
set superusers="test00"
Password test00 XXXX
EOF'
echo 'OR with  encrypted password like this:'

echo 'cat <<EOF
set superusers="test00"
Password_pbkdf2 test00 grub.pbkdf2.sha512.XXXX
EOF'

grub2-mkpasswd-pbkdf2
gedit /etc/grub.d/10_linux


echo 'Now generate the new grub.cfg file '
grub2-mkconfig --output=/boot/grub2/grub.cfg
grub2-mkconfig --output=/boot/efi/EFI/centos/grub.cfg


